/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ColorTemplate } from '@ohos/mpchart';
import { XAxis,XAxisPosition } from '@ohos/mpchart';
import {YAxis,AxisDependency,YAxisLabelPosition} from '@ohos/mpchart';
import { BarEntry } from '@ohos/mpchart';
import { JArrayList } from '@ohos/mpchart';
import { BarDataSet } from '@ohos/mpchart';
import { BarData } from '@ohos/mpchart';
import {BarChart,BarChartModel} from '@ohos/mpchart';
import type { IBarDataSet } from '@ohos/mpchart';
import title from '../title/index';
@Entry
@Component
struct TallBarChart {
  scroller: Scroller = new Scroller()
  @State model:BarChartModel = new BarChartModel();
  mWidth: number = 300; //表的宽度
  mHeight: number = 500; //表的高度
  minOffset: number = 15; //X轴线偏移量
  leftAxis: YAxis = null;
  rightAxis: YAxis = null;
  bottomAxis: XAxis = new XAxis();
  private menuItemArr:Array<string> =['View on GitHub'];
  private title:string ='ScrollViewAbility'
  public aboutToAppear(){
    let axisMaximum = Math.round(Math.random() * 10)+15
    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setAxisMinimum(0);
    this.leftAxis.setAxisMaximum(axisMaximum);

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(true);
    this.rightAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(axisMaximum);

    this.bottomAxis.setLabelCount(5, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setDrawGridLines(false)
    this.bottomAxis.setAxisMaximum(10);

    this.setData(this.bottomAxis.getAxisMaximum(),this.leftAxis.getAxisMaximum())

    this.model.mWidth = this.mWidth;
    this.model.mHeight = this.mHeight;
    this.model.init();
    this.model.setDrawBarShadow(false);
    this.model.setDrawValueAboveBar(true);
    this.model.getDescription().setEnabled(false);
    this.model.setMaxVisibleValueCount(16);
    this.model.setLeftYAxis(this.leftAxis);
    this.model.setRightYAxis(this.rightAxis);
    this.model.setXAxis(this.bottomAxis)
    this.model.mRenderer.initBuffers();
    this.model.prepareMatrixValuePx();

  }

  build() {

    Scroll(this.scroller) {
      Column() {
//        title({menuItemArr:this.menuItemArr,callBack:this.menuCallback,title:this.title})
        Text('START OF SCROLLVIEW').width('100%').height(px2vp(30)).textAlign(TextAlign.Center)
        Blank().height(px2vp(350))
        BarChart({model:this.model})
        Blank().height(px2vp(700))
        Text('END OF SCROLLVIEW').width('100%').height(px2vp(30)).textAlign(TextAlign.Center)
      }.width('100%').alignItems(HorizontalAlign.Start)
    }.width('100%').height('100%')


  }

  menuCallback(itemStr:string,index:number){
    console.info('callback1:'+itemStr+",index:"+index)
  }

  private setData(count: number, range: number) {

    let start: number = 1;

    let values:JArrayList<BarEntry> = new JArrayList<BarEntry>();

    for (let i = start; i < start + count; i++) {
      let multi = range;
      let val: number = Math.round(Math.random() * multi);
      values.add(new BarEntry(i, val))
    }

    let set1: BarDataSet;

    if (this.model.getBarData() != null &&
    this.model.getBarData().getDataSetCount() > 0) {
      set1 = this.model.getBarData().getDataSetByIndex(0) as BarDataSet;
      set1.setValues(values);
      this.model.getBarData().notifyDataChanged();
      this.model.notifyDataSetChanged();

    } else {
      set1 = new BarDataSet(values, "Data Set");

      set1.setColorsByVariable(ColorTemplate.VORDIPLOM_COLORS);
      set1.setDrawValues(false);

      let dataSets: JArrayList<IBarDataSet> = new JArrayList<IBarDataSet>();
      dataSets.add(set1);

      let data: BarData = new BarData(dataSets);


      this.model.setData(data);
      this.model.setFitBars(true);
    }
  }

}