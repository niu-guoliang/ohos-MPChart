/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import  {Legend,LegendForm} from '@ohos/mpchart';
import { LineChart,LineChartModel } from '@ohos/mpchart';
import {XAxis, XAxisPosition} from '@ohos/mpchart';
import {YAxis,AxisDependency, YAxisLabelPosition} from '@ohos/mpchart';
import { LineData } from '@ohos/mpchart';
import {LineDataSet,ColorStop,Mode} from '@ohos/mpchart';
import { EntryOhos } from '@ohos/mpchart';
import {JArrayList} from '@ohos/mpchart';
import type { ILineDataSet } from '@ohos/mpchart';
import {LimitLine,LimitLabelPosition} from '@ohos/mpchart';

@Entry
@Component
struct Index {
  topAxis: XAxis = new XAxis(); //顶部X轴
  bottomAxis: XAxis = new XAxis(); //底部X轴
  mWidth: number = 350; //表的宽度
  mHeight: number = 300; //表的高度
  minOffset: number = 15; //X轴线偏移量
  leftAxis: YAxis = null;
  rightAxis: YAxis = null;
  lineData: LineData = null;
  XLimtLine:LimitLine= new LimitLine(35, "Index 10");
  @State
  lineChartModel: LineChartModel = new LineChartModel()

  public aboutToAppear() {
    this.lineData = this.initCurveData(45, 100);

    this.topAxis.setLabelCount(6, false);
    this.topAxis.setPosition(XAxisPosition.TOP);
    this.topAxis.setAxisMinimum(0);
    this.topAxis.setAxisMaximum(200);

    this.bottomAxis.setLabelCount(6, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(200);

    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(11, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(-10);
    this.leftAxis.setAxisMaximum(100000);
    this.leftAxis.enableGridDashedLine(10,10,0)

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(7, false);
    this.rightAxis.setSpaceTop(11);
    this.rightAxis.setAxisMinimum(-10); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(100000);


    this.lineChartModel.setTopAxis(this.topAxis);
    this.lineChartModel.setBottomAxis(this.bottomAxis);
    this.lineChartModel.setWidth(this.mWidth);
    this.lineChartModel.setHeight(this.mHeight);
    this.lineChartModel.setMinOffset(this.minOffset);
    this.lineChartModel.setLeftAxis(this.leftAxis);
    this.lineChartModel.setRightAxis(this.rightAxis);
    this.lineChartModel.setLineData(this.lineData);
    this.lineChartModel.setIsShowLegend(false);
    this.lineChartModel.init();
  }

/**
   * 初始化数据
   * @param count  曲线图点的个数
   * @param range  y轴范围
   */
  private initCurveData(count: number, range: number): LineData {

    let values = new JArrayList<EntryOhos>();
    values.add(new EntryOhos(1, 60000));
    values.add(new EntryOhos(2, 59000));
    values.add(new EntryOhos(3, 60000));
    values.add(new EntryOhos(4, 60000));
    values.add(new EntryOhos(5, 60000));
    values.add(new EntryOhos(6, 59000));
    values.add(new EntryOhos(8, 61000));
    values.add(new EntryOhos(9, 59000));
    values.add(new EntryOhos(11, 60000));
    values.add(new EntryOhos(12, 59000));
    values.add(new EntryOhos(14, 60000));
    values.add(new EntryOhos(15, 60000));
    values.add(new EntryOhos(17, 60000));
    values.add(new EntryOhos(18, 60000));
    values.add(new EntryOhos(20, 60000));
    values.add(new EntryOhos(21, 59000));
    values.add(new EntryOhos(23, 61000));
    values.add(new EntryOhos(24, 59000));
    values.add(new EntryOhos(26, 60000));
    values.add(new EntryOhos(27, 59000));
    values.add(new EntryOhos(29, 60000));
    values.add(new EntryOhos(30, 59000));
    values.add(new EntryOhos(32, 61000));
    values.add(new EntryOhos(33, 14000));
    values.add(new EntryOhos(34, 52000));
    values.add(new EntryOhos(35, 57000));

    let values2 = new JArrayList<EntryOhos>();
    values2.add(new EntryOhos(10, 10));
    values2.add(new EntryOhos(60, 100));
    values2.add(new EntryOhos(110, 40));
    values2.add(new EntryOhos(160, 100));
    values2.add(new EntryOhos(200, 10));

    //    let values3 = new JArrayList<EntryOhos>();
    //    values3.add(new EntryOhos(30, 0));
    //    values3.add(new EntryOhos(80, 100));
    //    values3.add(new EntryOhos(130, 40));
    //    values3.add(new EntryOhos(180, 100));
    //    values3.add(new EntryOhos(230, 0));

    let gradientFillColor = new Array<ColorStop>();
    gradientFillColor.push([0x0C0099CC, 0.2])
    gradientFillColor.push([0x7F0099CC, 0.4])
    gradientFillColor.push([0x0099CC, 1.0])

    let dataSet = new JArrayList<ILineDataSet>();

    let set1 = new LineDataSet(values, "fps");
    set1.setDrawFilled(false);
    set1.setMode(Mode.CUBIC_BEZIER);
    set1.setGradientFillColor(gradientFillColor)
    set1.setColorByColor(Color.Green);
    set1.setLineWidth(1)
    set1.setDrawCircles(false);
    set1.setCircleColor(Color.Blue);
    set1.setCircleRadius(8);
    set1.setCircleHoleRadius(4)
    set1.setCircleHoleColor(Color.Green)
    set1.setDrawCircleHole(false)
    dataSet.add(set1);

    let set2 = new LineDataSet(values2, "DataSet 2");
    set2.setDrawFilled(false);
    set2.setMode(Mode.CUBIC_BEZIER);
    set2.setGradientFillColor(gradientFillColor)
    set2.setColorByColor(Color.Orange);
    set2.setLineWidth(4)
    set2.setDrawCircles(false);
    set2.setCircleColor(Color.Red);
    set2.setCircleRadius(8);
    set2.setDrawCircleHole(false);
    dataSet.add(set2);

    //    let set3 = new LineDataSet(values3, "DataSet 3");
    //    set3.setDrawFilled(false);
    //    set3.setGradientFillColor(gradientFillColor)
    //    set3.setColorByColor(Color.Red);
    //    set3.setLineWidth(2)
    //    set3.setDrawCircles(false);
    //    set3.setCircleColor(Color.Orange);
    //    set3.setCircleRadius(16);
    //    dataSet.add(set3);

    return new LineData(dataSet)
  }

  build() {
    Stack({ alignContent: Alignment.TopStart }) {
      LineChart({lineChartModel: $lineChartModel})
    }
  }
}