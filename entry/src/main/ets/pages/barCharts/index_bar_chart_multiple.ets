/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Color }  from '@ohos/mpchart';
import { XAxis,XAxisPosition } from '@ohos/mpchart'
import {YAxis,AxisDependency,YAxisLabelPosition} from '@ohos/mpchart'
import { BarEntry } from '@ohos/mpchart';
import { JArrayList } from '@ohos/mpchart';
import { BarDataSet } from '@ohos/mpchart';
import { BarData } from '@ohos/mpchart';
import {BarChart,BarChartModel} from '@ohos/mpchart'
import type { IBarDataSet } from '@ohos/mpchart'
import title from '../title/index';

@Entry
@Component
struct Index_bar_chart_multiple {
  @State model:BarChartModel = new BarChartModel();
  mWidth: number = 350; //表的宽度
  mHeight: number = 500; //表的高度
  minOffset: number = 15; //X轴线偏移量
  leftAxis: YAxis = null;
  rightAxis: YAxis = null;
  bottomAxis: XAxis = new XAxis();
  //标题栏菜单文本
  private menuItemArr: Array<string> = ['Toggle Bar Borders','Animate X','Animate Y','Animate XY'];
  //标题栏标题
  private title: string = 'BarChart'
  @State @Watch("menuCallback") titleModel: title.Model = new title.Model()
  //标题栏菜单回调
  menuCallback(){
    if (this.titleModel == null || this.titleModel == undefined) {
      return
    }
    let index: number = this.titleModel.getIndex()
    if(index==undefined||index==-1){
      return
    }
    switch (this.menuItemArr[index]) {
      case 'Toggle Bar Borders':
        for(let i = 0;i < this.model.getBarData().getDataSets().length();i++){
          let barDataSet = this.model.getBarData().getDataSets().get(i) as BarDataSet;
          barDataSet.setBarBorderWidth(barDataSet.getBarBorderWidth() == 1?0:1)
        }
        this.model.invalidate()
        break;
      case 'Animate X':
        this.model.animateX(2000)
        break;
      case 'Animate Y':
        this.model.animateY(2000)
        break;
      case 'Animate XY':
        this.model.animateXY(2000,2000)
        break;

    }
    this.titleModel.setIndex(-1)
  }

  public aboutToAppear(){
    this.titleModel.menuItemArr = this.menuItemArr
    this.titleModel.title = this.title
    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(6, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(0);
    this.leftAxis.setAxisMaximum(110);
    this.leftAxis.enableGridDashedLine(10,10,0)

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(6, false);
    this.rightAxis.setSpaceTop(11);
    this.rightAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(110);

    this.bottomAxis.setLabelCount(5, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(5);

    this.setData(this.bottomAxis.getAxisMaximum(),this.leftAxis.getAxisMaximum())

    this.model.mWidth = this.mWidth;
    this.model.mHeight = this.mHeight;
    this.model.init();
    this.model.setDrawBarShadow(false);
    this.model.setDrawValueAboveBar(true);
    this.model.getDescription().setEnabled(false);
    this.model.setMaxVisibleValueCount(60);
    this.model.setLeftYAxis(this.leftAxis);
    this.model.setRightYAxis(this.rightAxis);
    this.model.setXAxis(this.bottomAxis)
    this.model.mRenderer.initBuffers();
    this.model.prepareMatrixValuePx();
  }

  build() {
    Column(){
      title({ model: this.titleModel })
      Stack(){
        BarChart({model:this.model})
      }
    }.alignItems(HorizontalAlign.Start)
  }

  private setData(count: number, range: number) {

    let groupSpace: number = 0.08;
    let barSpace: number = 0.03; // x4 DataSet
    let barWidth: number = 0.2; // x4 DataSet
    // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"

    let groupCount: number = count + 1;
    let startYear: number = 1980;
    let endYear: number = startYear + groupCount;

    let values1:JArrayList<BarEntry> = new JArrayList<BarEntry>();
    let values2:JArrayList<BarEntry> = new JArrayList<BarEntry>();
    let values3:JArrayList<BarEntry> = new JArrayList<BarEntry>();
    let values4:JArrayList<BarEntry> = new JArrayList<BarEntry>();

    let randomMultiplier: number = range;

    for (let i = startYear; i < endYear; i++) {
      values1.add(new BarEntry(i, (Math.random() * randomMultiplier)))
      values2.add(new BarEntry(i, (Math.random() * randomMultiplier)))
      values3.add(new BarEntry(i, (Math.random() * randomMultiplier)))
      values4.add(new BarEntry(i, (Math.random() * randomMultiplier)))
    }

    let set1: BarDataSet,set2: BarDataSet,set3: BarDataSet,set4: BarDataSet;

    if (this.model.getBarData() != null &&
    this.model.getBarData().getDataSetCount() > 0) {
      set1 = this.model.getBarData().getDataSetByIndex(0) as BarDataSet;
      set2 = this.model.getBarData().getDataSetByIndex(1) as BarDataSet;
      set3 = this.model.getBarData().getDataSetByIndex(2) as BarDataSet;
      set4 = this.model.getBarData().getDataSetByIndex(3) as BarDataSet;
      set1.setValues(values1);
      set2.setValues(values2);
      set3.setValues(values3);
      set4.setValues(values4);
      this.model.getBarData().notifyDataChanged();
      this.model.notifyDataSetChanged();

    } else {
      let colors: number[] = [Color.rgb(104, 241, 175),Color.rgb(164, 228, 251),Color.rgb(242, 247, 158),Color.rgb(255, 102, 0)];
      set1 = new BarDataSet(values1, "Company A");
      set1.setColorsByArr(colors);
      set2 = new BarDataSet(values2, "Company B");
      set2.setColorsByArr(colors);
      set3 = new BarDataSet(values3, "Company C");
      set3.setColorsByArr(colors);
      set4 = new BarDataSet(values2, "Company D");
      set4.setColorsByArr(colors);

      let dataSets: JArrayList<IBarDataSet> = new JArrayList<IBarDataSet>();
      dataSets.add(set1);
      dataSets.add(set2);
      dataSets.add(set3);
      dataSets.add(set4);

      let data: BarData = new BarData(dataSets);


      this.model.setData(data);
    }
    this.model.getBarData().setBarWidth(barWidth);
    this.bottomAxis.setAxisMinimum(startYear);
    this.bottomAxis.setAxisMaximum(startYear + this.model.getBarData().getGroupWidth(groupSpace, barSpace) * groupCount);

    this.model.groupBars(startYear, groupSpace, barSpace);
  }
}