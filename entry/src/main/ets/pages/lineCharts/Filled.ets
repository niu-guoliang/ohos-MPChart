/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {ColorTemplate} from '@ohos/mpchart'
import { LineChart,LineChartModel } from '@ohos/mpchart'
import {XAxis, XAxisPosition} from '@ohos/mpchart'
import {YAxis,AxisDependency, YAxisLabelPosition} from '@ohos/mpchart'
import { LineData } from '@ohos/mpchart'
import {LineDataSet,ColorStop,Mode,FillStyle} from '@ohos/mpchart'
import { EntryOhos } from '@ohos/mpchart'
import {JArrayList} from '@ohos/mpchart'
import type { ILineDataSet } from '@ohos/mpchart'
@Entry
@Component
struct Filled {
  topAxis: XAxis = new XAxis(); //顶部X轴
  bottomAxis: XAxis = new XAxis(); //底部X轴
  mWidth: number = 350; //表的宽度
  mHeight: number = 300; //表的高度
  minOffset: number = 15; //X轴线偏移量
  leftAxis: YAxis = null;
  rightAxis: YAxis = null;
  lineData: LineData = null;
  @State
  lineChartModel: LineChartModel = new LineChartModel();

  aboutToAppear() {
    this.lineData = this.initCurveData(100, 60);

    this.topAxis.setLabelCount(5, false);
    this.topAxis.setPosition(XAxisPosition.TOP);
    this.topAxis.setAxisMinimum(this.lineData.getXMin());
    this.topAxis.setAxisMaximum(this.lineData.getXMax());
    this.topAxis.setDrawAxisLine(true);
    this.topAxis.setDrawLabels(false);
    this.topAxis.setDrawGridLines(false);

    this.bottomAxis.setLabelCount(5, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(this.lineData.getXMin());
    this.bottomAxis.setAxisMaximum(this.lineData.getXMax());
    this.bottomAxis.setDrawAxisLine(true);
    this.bottomAxis.setDrawLabels(false)

    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(6, false);
    this.leftAxis.setDrawGridLines(false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setAxisMinimum(-250);
    this.leftAxis.setAxisMaximum(900);
    this.leftAxis.setDrawAxisLine(true);

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(6, false);
    this.rightAxis.setAxisMinimum(-250); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(900);
    this.rightAxis.setDrawAxisLine(true);
    this.rightAxis.setDrawLabels(false);

    this.lineChartModel.setChartBgColor(ColorTemplate.argb(150, 51, 181, 229));
    this.lineChartModel.setTopAxis(this.topAxis);
    this.lineChartModel.setBottomAxis(this.bottomAxis);
    this.lineChartModel.setWidth(this.mWidth);
    this.lineChartModel.setHeight(this.mHeight);
    this.lineChartModel.setMinOffset(this.minOffset);
    this.lineChartModel.setLeftAxis(this.leftAxis);
    this.lineChartModel.setRightAxis(this.rightAxis);
    this.lineChartModel.setLineData(this.lineData);
    this.lineChartModel.setIsShowLegend(false);
    this.lineChartModel.init();

  }

/**
   * 初始化数据
   * @param count  曲线图点的个数
   * @param range  y轴范围
   */
  private initCurveData(count: number, range: number): LineData {

    let values1 = new JArrayList<EntryOhos>();
    for (let i = 0; i < count; i++) {
      let val: number = Math.random() * range + 50;
      values1.add(new EntryOhos(i, val));
    }

    let values2 = new JArrayList<EntryOhos>();
    for (let i = 0; i < count; i++) {
      let val: number = Math.random() * range + 450;
      values2.add(new EntryOhos(i, val));
    }

    let gradientFillColor1 = new Array<ColorStop>();
    gradientFillColor1.push(['#ffffffff', 0.1])
    gradientFillColor1.push(['#ffffffff', 1.0])

    let gradientFillColor2 = new Array<ColorStop>();
    gradientFillColor2.push(['#ffffffff', 0.1])
    gradientFillColor2.push(['#ffffffff', 1.0])

    let dataSet = new JArrayList<ILineDataSet>();

    let set1 = new LineDataSet(values1, "DataSet 1");
    set1.setDrawFilled(true);
    set1.setDrawValues(false);
    set1.setDrawCircles(false);
    set1.setMode(Mode.LINEAR);
    set1.setFillStyle(FillStyle.MIN);
    set1.setGradientFillColor(gradientFillColor1);
    set1.setColorByColor(ColorTemplate.colorRgb(255, 241, 46));
    set1.setLineWidth(2)
    dataSet.add(set1);

    let set2 = new LineDataSet(values2, "DataSet 2");
    set2.setDrawFilled(true);
    set2.setDrawValues(false);
    set2.setDrawCircles(false);
    set2.setMode(Mode.LINEAR);
    set2.setFillStyle(FillStyle.MAX);
    set2.setGradientFillColor(gradientFillColor2);
    set2.setColorByColor(ColorTemplate.colorRgb(255, 241, 46));
    set2.setLineWidth(2)
    dataSet.add(set2);

    return new LineData(dataSet)
  }


  build() {
    Column() {
      Stack({ alignContent: Alignment.TopStart }) {
        LineChart({lineChartModel: $lineChartModel})
      }
    }
  }
}
