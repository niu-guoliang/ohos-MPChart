/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import LimitLine,{LimitLabelPosition} from '../components/LimitLine';
import { JArrayList } from '../utils/JArrayList';
import MyRect from '../data/Rect';
import Paint, { LinePaint , TextPaint, PathPaint,Style}  from '../data/Paint';
import MPPointD from '../utils/MPPointD';
import Transformer from '../utils/Transformer';
import YAxis from '../components/YAxis';
import ViewPortHandler from '../utils/ViewPortHandler';
import YAxisRenderer from './YAxisRenderer';
import Utils from '../utils/Utils'
import {AxisDependency,YAxisLabelPosition} from '../components/YAxis'

export default class YAxisRendererHorizontalBarChart extends YAxisRenderer {

  constructor( viewPortHandler:ViewPortHandler,  yAxis:YAxis,
               trans:Transformer ) {
    super(viewPortHandler, yAxis, trans);

    this.mLimitLinePaint.setTextAlign(TextAlign.Start);
  }

  /**
   * Computes the axis values.
   *
   * @param yMin - the minimum y-value in the data object for this axis
   * @param yMax - the maximum y-value in the data object for this axis
   */
  public computeAxis( yMin: number, yMax: number, inverted:boolean) {

    // calculate the starting and entry point of the y-labels (depending on
    // zoom / contentrect bounds)
    if (this.mViewPortHandler.contentHeight() > 10 && !this.mViewPortHandler.isFullyZoomedOutX()) {

      let p1:MPPointD = this.mTrans.getValuesByTouchPoint(this.mViewPortHandler.contentLeft(),
      this.mViewPortHandler.contentTop());
      let p2:MPPointD = this.mTrans.getValuesByTouchPoint(this.mViewPortHandler.contentRight(),
      this.mViewPortHandler.contentTop());

      if (!inverted) {
        yMin =  p1.x;
        yMax =  p2.x;
      } else {
        yMin =  p2.x;
        yMax =  p1.x;
      }

      MPPointD.recycleInstance(p1);
      MPPointD.recycleInstance(p2);
    }

    this.computeAxisValues(yMin, yMax);
  }

  /**
   * draws the y-axis labels to the screen
   */
  public renderAxisLabels():Paint[] {

    let paints:Paint[] = [];

    if (!this.mYAxis.isEnabled() || !this.mYAxis.isDrawLabelsEnabled())
    return;

    let positions: number[] = this.getTransformedPositions();

    this.mAxisLabelPaint.setTypeface(this.mYAxis.getTypeface());
    this.mAxisLabelPaint.setTextSize(this.mYAxis.getTextSize());
    this.mAxisLabelPaint.setColor(this.mYAxis.getTextColor());
    this.mAxisLabelPaint.setTextAlign(TextAlign.Center);

    let baseYOffset = Utils.convertDpToPixel(2.5);
    let textHeight = Utils.calcTextHeight(this.mAxisLabelPaint, "Q");

    let dependency:AxisDependency = this.mYAxis.getAxisDependency();
    let labelPosition:YAxisLabelPosition = this.mYAxis.getLabelPosition();

    let yPos:number = 0;

    if (dependency == AxisDependency.LEFT) {

      if (labelPosition == YAxisLabelPosition.OUTSIDE_CHART) {
        yPos = this.mViewPortHandler.contentTop() - baseYOffset;
      } else {
        yPos = this.mViewPortHandler.contentTop() - baseYOffset;
      }

    } else {

      if (labelPosition == YAxisLabelPosition.OUTSIDE_CHART) {
        yPos = this.mViewPortHandler.contentBottom() + textHeight + baseYOffset;
      } else {
        yPos = this.mViewPortHandler.contentBottom() + textHeight + baseYOffset;
      }
    }

    return paints.concat(this.drawYLabels(yPos, positions, this.mYAxis.getYOffset()))
  }

  public renderAxisLine(): Paint[] {

    if (!this.mYAxis.isEnabled() || !this.mYAxis.isDrawAxisLineEnabled())
    return;

    let paints: Paint[] = [];

    this.mAxisLinePaint.setColor(this.mYAxis.getAxisLineColor());
    this.mAxisLinePaint.setStrokeWidth(this.mYAxis.getAxisLineWidth());

    if (this.mYAxis.getAxisDependency() == AxisDependency.LEFT) {
      let linePaint:LinePaint = new LinePaint();
      linePaint.set(this.mAxisLinePaint);
      linePaint.setStartPoint([this.mViewPortHandler.contentLeft(),this.mViewPortHandler.contentTop()])
      linePaint.setEndPoint([this.mViewPortHandler.contentRight(),this.mViewPortHandler.contentTop()])
      paints = paints.concat(linePaint);
    } else {
      let linePaint:LinePaint = new LinePaint();
      linePaint.set(this.mAxisLinePaint);
      linePaint.setStartPoint([this.mViewPortHandler.contentLeft(),this.mViewPortHandler.contentBottom()])
      linePaint.setEndPoint([this.mViewPortHandler.contentRight(),this.mViewPortHandler.contentBottom()])
      paints = paints.concat(linePaint);
    }
    return paints;
  }

  /**
   * draws the y-labels on the specified x-position
   *
   * @param fixedPosition
   * @param positions
   */
  protected drawYLabels( fixedPosition:number, positions:number[], offset:number) {

    let paints:Paint[] = [];
    this.mAxisLabelPaint.setTypeface(this.mYAxis.getTypeface());
    this.mAxisLabelPaint.setTextSize(this.mYAxis.getTextSize());
    this.mAxisLabelPaint.setColor(this.mYAxis.getTextColor());

    const fromIndex = this.mYAxis.isDrawBottomYLabelEntryEnabled() ? 0 : 1;
    const to = this.mYAxis.isDrawTopYLabelEntryEnabled()
      ? this.mYAxis.mEntryCount
      : (this.mYAxis.mEntryCount - 1);

    let xOffset:number = this.mYAxis.getLabelXOffset();

    for (let i = fromIndex; i < to; i++) {

      let text:string = this.mYAxis.getFormattedLabel(i);
      let textPaint:TextPaint = new TextPaint();
      textPaint.set(this.mAxisLabelPaint)
      textPaint.setText(text);
      textPaint.setX(positions[i * 2]);
      textPaint.setY(fixedPosition - offset + xOffset)
      paints.push(textPaint)
    }
    return paints;
  }

  protected getTransformedPositions():number[] {

    if(this.mGetTransformedPositionsBuffer.length != this.mYAxis.mEntryCount * 2) {
      this.mGetTransformedPositionsBuffer = new Array<number>(this.mYAxis.mEntryCount * 2);
    }
    let positions:number[] = this.mGetTransformedPositionsBuffer;

    for (let i = 0; i < positions.length; i += 2) {
      // only fill x values, y values are not needed for x-labels
      positions[i] = this.mYAxis.mEntries[i / 2];
    }

    this.mTrans.pointValuesToPixel(positions);
    return positions;
  }

  public getGridClippingRect():MyRect {
    this.mGridClippingRect.set(this.mViewPortHandler.getContentRect().left,this.mViewPortHandler.getContentRect().top
      ,this.mViewPortHandler.getContentRect().right,this.mViewPortHandler.getContentRect().bottom);
    this.mGridClippingRect.inset(-this.mAxis.getGridLineWidth(), 0,-this.mAxis.getGridLineWidth(),0);
    return this.mGridClippingRect;
  }

  protected linePath( p:string, i:number, positions:number[]):string {

    p += "M"+positions[i]+" "+this.mViewPortHandler.contentTop()+" L"+positions[i]+" "+this.mViewPortHandler.contentBottom()
    return p;
  }

  protected mDrawZeroLinePathBuffer:string = "";

  protected drawZeroLine():Paint {

    this.mZeroLineClippingRect.set(this.mViewPortHandler.getContentRect().left,this.mViewPortHandler.getContentRect().top,
      this.mViewPortHandler.getContentRect().right,this.mViewPortHandler.getContentRect().bottom);

    this.mZeroLineClippingRect.inset(-this.mYAxis.getZeroLineWidth(), 0,-this.mYAxis.getZeroLineWidth(),0);

    // draw zero line
    let pos:MPPointD = this.mTrans.getPixelForValues(0, 0);

    this.mZeroLinePaint.setColor(this.mYAxis.getZeroLineColor());
    this.mZeroLinePaint.setStrokeWidth(this.mYAxis.getZeroLineWidth());

    let zeroLinePath:string = this.mDrawZeroLinePathBuffer;
    zeroLinePath = "";

    zeroLinePath = "M"+(pos.x - 1)+" "+this.mViewPortHandler.contentTop() +" L"+(pos.x - 1)+" "+this.mViewPortHandler.contentBottom();

    let pathPaint:PathPaint = new PathPaint();
    pathPaint.set(this.mZeroLinePaint)
    pathPaint.setCommands(zeroLinePath);

    return pathPaint
  }

  protected mRenderLimitLinesPathBuffer:string = "";
  protected mRenderLimitLinesBuffer:Array<number> = new Array<number>(4);

  /**
   * Draws the LimitLines associated with this axis to the screen.
   * This is the standard XAxis renderer using the YAxis limit lines.
   *
   * @param c
   */
  public renderLimitLines():Paint[] {

    let paints:Paint[] = [];
    let limitLines:JArrayList<LimitLine> = this.mYAxis.getLimitLines();

    if (limitLines == null || limitLines.size() <= 0)
    return;

    let pts:number[] = this.mRenderLimitLinesBuffer;
    pts[0] = 0;
    pts[1] = 0;
    pts[2] = 0;
    pts[3] = 0;
    let limitLinePath:string = this.mRenderLimitLinesPathBuffer;
    limitLinePath = "";

    for (let i = 0; i < limitLines.size(); i++) {

      let l:LimitLine = limitLines.get(i);

      if (!l.isEnabled())
      continue;

      this.mLimitLineClippingRect.set(this.mViewPortHandler.getContentRect().left,this.mViewPortHandler.getContentRect().top,
        this.mViewPortHandler.getContentRect().right,this.mViewPortHandler.getContentRect().bottom);
      this.mLimitLineClippingRect.inset(-l.getLineWidth(), 0,-l.getLineWidth(),0);

      pts[0] = l.getLimit();
      pts[2] = l.getLimit();

      this.mTrans.pointValuesToPixel(pts);

      pts[1] = this.mViewPortHandler.contentTop();
      pts[3] = this.mViewPortHandler.contentBottom();

      limitLinePath = "M"+pts[0]+" "+pts[1]+" L"+pts[2]+" "+pts[3];

      this.mLimitLinePaint.setStyle(Style.STROKE);
      this.mLimitLinePaint.setColor(l.getLineColor());
      this.mLimitLinePaint.setDashPathEffect(l.getDashPathEffect());
      this.mLimitLinePaint.setStrokeWidth(l.getLineWidth());

      let pathPaint:PathPaint = new PathPaint();
      pathPaint.set(this.mLimitLinePaint);
      pathPaint.setCommands(limitLinePath);
      paints.push(pathPaint)
      let label:string = l.getLabel();

      // if drawing the limit-value label is enabled
      if (label != null && label != "") {

        this.mLimitLinePaint.setStyle(l.getTextStyle());
        this.mLimitLinePaint.setDashPathEffect(null);
        this.mLimitLinePaint.setColor(l.getTextColor());
        this.mLimitLinePaint.setTypeface(l.getTypeface());
        this.mLimitLinePaint.setStrokeWidth(0.5);
        this.mLimitLinePaint.setTextSize(l.getTextSize());

        let xOffset:number = l.getLineWidth() + l.getXOffset();
        let yOffset:number = Utils.convertDpToPixel(2) + l.getYOffset();

        const  position:LimitLabelPosition = l.getLabelPosition();

        if (position == LimitLabelPosition.RIGHT_TOP) {

          const labelLineHeight:number = Utils.calcTextHeight(this.mLimitLinePaint, label);
          this.mLimitLinePaint.setTextAlign(TextAlign.Start);
          let textPaint:TextPaint = new TextPaint();
          textPaint.set(this.mLimitLinePaint)
          textPaint.setText(label)
          textPaint.setX(pts[0] + xOffset)
          textPaint.setY(this.mViewPortHandler.contentTop() + yOffset + labelLineHeight)
          paints.push(textPaint);
        } else if (position == LimitLabelPosition.RIGHT_BOTTOM) {

          this.mLimitLinePaint.setTextAlign(TextAlign.Start);
          let textPaint:TextPaint = new TextPaint();
          textPaint.set(this.mLimitLinePaint)
          textPaint.setText(label)
          textPaint.setX(pts[0] + xOffset)
          textPaint.setY(this.mViewPortHandler.contentBottom() - yOffset)
          paints.push(textPaint);
        } else if (position == LimitLabelPosition.LEFT_TOP) {

          this.mLimitLinePaint.setTextAlign(TextAlign.End);
          const labelLineHeight:number = Utils.calcTextHeight(this.mLimitLinePaint, label);
          this.mLimitLinePaint.setTextAlign(TextAlign.Start);
          let textPaint:TextPaint = new TextPaint();
          textPaint.set(this.mLimitLinePaint)
          textPaint.setText(label)
          textPaint.setX(pts[0] - xOffset)
          textPaint.setY(this.mViewPortHandler.contentTop() + yOffset + labelLineHeight)
          paints.push(textPaint);
        } else {

          this.mLimitLinePaint.setTextAlign(TextAlign.End);
          let textPaint:TextPaint = new TextPaint();
          textPaint.set(this.mLimitLinePaint)
          textPaint.setText(label)
          textPaint.setX(pts[0] - xOffset)
          textPaint.setY(this.mViewPortHandler.contentBottom() - yOffset)
          paints.push(textPaint);
        }
      }
    }
    return paints;
  }
}
