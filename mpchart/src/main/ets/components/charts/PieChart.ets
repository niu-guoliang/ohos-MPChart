/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { PathPaint } from '../data/Paint';
import PieData from '../data/PieData';
import MyPieChartRender from '../renderer/MyPieChartRender'
import { JArrayList } from '../utils/JArrayList';
import Utils from '../utils/Utils';
import MPPointF from '../utils/MPPointF';
import LegendEntry from '../components/LegendEntry';
import LegendView from '../components/LegendView';
import ViewPortHandler from '../utils/ViewPortHandler'
import ChartAnimator from '../animation/ChartAnimator'
import Legend, { LegendForm, LegendVerticalAlignment, LegendOrientation, LegendHorizontalAlignment
} from '../components/Legend';
import { ImagePaint } from '../data/Paint';


@Component
struct PieChart {
  @Link model: PieChart.Model

  build() {
    Stack({ alignContent: Alignment.TopStart }) {
      Column() {
        LegendView({
          model: this.model.legendModel
        })
      }
      .width('100%')
      .height('100%')
      .zIndex(1000)
      .visibility(this.model.isShowLegend ? Visibility.Visible : Visibility.None)

      ForEach(this.model.outerPaths, (item: PathPaint) => {
        Stack() {
          Path()
            .width('100%')
            .height('100%')
            .commands(item.commands)
            .fill(item.filledColor)
            .position({ x: this.model.offset.x, y: this.model.offset.y })

          Path()
            .width('100%')
            .height('100%')
            .commands(item.lineSvg)
            .stroke(this.model.lineColor)
            .strokeWidth(1)
            .fill('none')
            .zIndex(1000)
            .position({ x: this.model.offset.x, y: this.model.offset.y })
            .visibility(this.model.mUsePercentValues ? Visibility.Visible : Visibility.None)

          Column() {
            Text(item.percentage)
              .fontSize(12)
              .fontColor(this.model.valueTextColor)
              .zIndex(3)
          }
          .zIndex(1000)
          .align(Alignment.Start)
          .position({
            x: this.model.offset.x + px2vp(item.positionX),
            y: this.model.offset.y + px2vp(item.positionY)
          })
          .visibility(this.model.mUsePercentValues ? Visibility.Visible : Visibility.None)

          if (this.model.mDrawEntryLabels) {
            Column() {
              Text(item.label)
                .fontSize(12)
                .fontColor(this.model.labelColor)
                .zIndex(3)
            }.align(Alignment.Start)
            .zIndex(1000)
            .position({
              x: px2vp(item.labelX) + this.model.offset.x,
              y: px2vp(item.labelY) + this.model.offset.y
            })
          }

          Image(this.model.iconPath)
            .width(this.model.imageWidth)
            .height(this.model.imageHeight)
            .position({
              x: this.model.offset.x + px2vp(item.iconX),
              y: this.model.offset.y + px2vp(item.iconY)
            })
            .visibility(this.model.pieData.getDataSet().isDrawIconsEnabled() ? Visibility.Visible : Visibility.None)
        }

      }, item => JSON.stringify(item))
      Path()
        .height(1)
        .commands(this.model.innerPath)
        .fill(Color.White)
        .stroke(this.model.circleColor)
        .strokeWidth(8)
        .strokeOpacity(0.5)
        .zIndex(900)
        .margin({ left: this.model.offset.x, top: this.model.offset.y })
        .visibility(this.model.mDrawHole ? Visibility.Visible : Visibility.None)
      Text("mpchart")
        .fontSize(12)
        .zIndex(950)
        .position({ x: -40, y: 0 })
        .padding({ left: this.model.offset.x, top: this.model.offset.y })
        .visibility(this.model.mDrawCenterText ? Visibility.Visible : Visibility.None)
    }
    .gesture(GestureGroup(GestureMode.Exclusive,
    TapGesture({ count: 1, fingers: 1 })
      .onAction((event: GestureEvent) => {
        let angle = this.model.getAngleForPoint(event.fingerList[0].localX, event.fingerList[0].localY)
        this.model.drawMarkers(angle, event.fingerList[0].localX, event.fingerList[0].localY)
        this.model.init()
      }),
    SwipeGesture()
      .onAction((event: GestureEvent) => {
        if (this.model.isRotationEnabled()) {
          this.model.mRotationAngle = this.model.mRotationAngle + event.angle
          this.model.init()
        }
      })
    ))
    .width('100%')
  }

  aboutToAppear() {
    this.model.legendModel.setLegend(this.model.legend)
      .setWidth(this.model.legendWidth)
      .setHeight(this.model.legendHeight)
    // 初始化孔数据
    this.model.calHolePath()
    this.model.init()
    let imagePaint: ImagePaint = this.model.pieData.getDataSet()
      .getEntryForIndex(0)
      .getIcon();
    if (imagePaint != undefined) {
      this.model.iconPath = imagePaint.getIcon()
    }
  }
}

namespace PieChart {
  @Observed
  export class Model {
    legendModel: LegendView.Model = new LegendView.Model()
    isShowLegend: boolean = true
    pieData: PieData = null;
    //如果为真，将绘制图表内的白洞
    mDrawHole: boolean = true
    // 如果为真，则孔将透视到切片的内部尖端
    mDrawSlicesUnderHole: boolean = false

    // 如果为真，则饼图的切片是四舍五入的
    mDrawRoundedSlices: boolean = false
    // 外圈的svg路径
    outerPaths: Array<PathPaint> = new Array<PathPaint>()
    //表示饼图中心孔的大小百分比,默认半径除以2
    mHoleRadiusPercent: number = 0.5
    // 内圈的svg路径
    innerPath: string = ''
    linePath: string = ''
    labelColor: number = Color.White
    // 扇形的角度数组
    drawAngles: number[] = [90, 90, 90, 40, 10, 20, 20]
    // 半径
    radius: number = 100
    pathPaints: Array<PathPaint> = new Array<PathPaint>()
    angleFlag: number
    legendWidth: number = 300; //legend的宽度
    legendHeight: number = 50; //legend的高度
    legend: Legend = new Legend();
    // 是否绘制值
    mDrawEntryLabels: boolean = true
    valueTextColor: number = Color.White
    mMaxAngle: number = 360;
    lineColor: number
    // 透明圈的颜色
    circleColor: number = Color.White
    mViewPortHandler: ViewPortHandler = new ViewPortHandler();
    mAnimator: ChartAnimator = new ChartAnimator()
    //偏移
    offset: MPPointF = new MPPointF(0, 0)
    // 透明圈宽度
    TransparentCircleWidth: number = 8
    // 高亮下标
    mIndicesToHighlight: number
    // 需要高亮第几组数据
    count: number = -1
    // 原始旋转角度
    mRawRotationAngle: number = 0
    // holds the normalized version of the current rotation angle of the chart
    mRotationAngle: number = 0
    // 拖动开始的角度
    mStartAngle: number = 0
    // 如果为 true，则饼图中的值将绘制为百分比值
    mUsePercentValues: boolean = false
    // 如果启用，则绘制中心文本
    mDrawCenterText: boolean = true
    // 是否启用旋转的标志
    mRotateEnabled: boolean = true
    // 保存每个切片的绝对角度的数组
    mAbsoluteAngles: number[] = []
    imageWidth: number = 20
    imageHeight: number = 20
    iconPath: string | Resource
    mMinAngleForSlices: number = 0
	extraOffsetYDistance = 0
    extraOffsetXDistance = 0

    setExtraOffsetYDistance(offsetY: number) {
      this.extraOffsetYDistance = offsetY
      return this
    }

    setExtraOffsetXDistance(offsetX: number) {
      this.extraOffsetXDistance = offsetX
      return this
    }

    getMinAngleForSlices(): number {
      return this.mMinAngleForSlices
    }

    setMinAngleForSlices(minAngle: number): Model {
      if (minAngle > (this.mMaxAngle / 2)) {
        minAngle = this.mMaxAngle / 2
      } else if (minAngle < 0) {
        minAngle = 0
      }
      this.mMinAngleForSlices = minAngle
      return this
    }

    // 设置是否以弯曲方式绘制切片，仅在启用绘制孔并且切片未在孔下方绘制时有效。
    setDrawRoundedSlices(enabled: boolean): Model {
      this.mDrawRoundedSlices = enabled
      return this
    }

    isDrawRoundedSlicesEnabled(): boolean {
      return this.mDrawRoundedSlices
    }

    setImageWidth(imageWidth: number): Model{
      this.imageWidth = imageWidth
      return this
    }

    setImageHeight(imageHeight: number): Model{
      this.imageHeight = imageHeight
      return this
    }

    getImageHeight(): number{
      return this.imageHeight
    }

    getImageWidth(): number{
      return this.imageWidth
    }

    getAbsoluteAngles(): number[] {
      return this.mAbsoluteAngles
    }

    setRotationEnabled(enabled: boolean): Model {
      this.mRotateEnabled = enabled
      return this
    }

    isRotationEnabled(): boolean {
      return this.mRotateEnabled;
    }

    isDrawCenterTextEnabled(): boolean{
      return this.mDrawCenterText
    }

    setDrawCenterText(enabled: boolean): Model {
      this.mDrawCenterText = enabled
      return this
    }

    isDrawEntryLabelsEnabled(): boolean {
      return this.mDrawEntryLabels
    }

    setDrawSliceText(enabled: boolean): Model {
      this.mDrawEntryLabels = enabled
      return this
    }

    setUsePercentValues(enabled: boolean): Model {
      this.mUsePercentValues = enabled
      return this
    }

    isUsePercentValuesEnabled(): boolean {
      return this.mUsePercentValues;
    }

    getRotationAngle(): number {
      return this.mRotationAngle
    }

    setRotationAngle(angle: number): Model{
      this.mRawRotationAngle = angle;
      this.mRotationAngle = Utils.getNormalizedAngle(this.mRawRotationAngle)
      this.mRotationAngle = this.mRotationAngle
      return this
    }

    getIndices(): number{
      return this.mIndicesToHighlight = this.mIndicesToHighlight
    }

    setIndices(mIndicesToHighlight: number): Model{
      this.mIndicesToHighlight = mIndicesToHighlight
      return this
    }

    setTransparentCircleWidth(width: number): Model{
      this.TransparentCircleWidth = width
      return this
    }

    getOffset(): MPPointF {
      return this.offset
    }

    setOffset(offset: MPPointF): Model{
      this.offset = offset
      return this
    }

    setShowLegend(isShowLegend: boolean): Model{
      this.isShowLegend = isShowLegend
      return this
    }

    setLineColor(lineColor: number): Model{
      this.lineColor = lineColor
      return this
    }

    setLabelColor(labelColor: number) {
      this.labelColor = labelColor
      return this
    }

    getLabelColor(): number{
      return this.labelColor
    }

    setLinePath(linePath: string): Model{
      this.linePath = linePath
      return this
    }

    setMaxAngle(mMaxAngle: number): Model{
      if (this.mMaxAngle > 360) {
        this.mMaxAngle = 360;
      }
      if (this.mMaxAngle < 90) {
        this.mMaxAngle = 90;
      }
      this.mMaxAngle = mMaxAngle
      return this
    }

    getMaxAngle(): number{
      return this.mMaxAngle
    }

    setValueTextColor(color: number): Model{
      this.valueTextColor = color
      return this
    }

    setRadius(radius: number): Model{
      this.radius = radius
      return this
    }

    getRadius(): number{
      return this.radius
    }

    setPieData(pieData: PieData): Model{
      this.pieData = pieData
      return this
    }

    getPieData(): PieData{
      return this.pieData
    }

    setDrawAngles(drawAngles: number[]): Model{
      this.drawAngles = drawAngles
      return this
    }

    getDrawAngles(): number[]{
      return this.drawAngles
    }

    isDrawHoleEnabled(): boolean{
      return this.mDrawHole
    }

    isDrawSlicesUnderHoleEnabled(): boolean {
      return this.mDrawSlicesUnderHole;
    }

    getHoleRadius(): number {
      return this.mHoleRadiusPercent;
    }

    setHoleRadius(mHoleRadiusPercent: number): Model {
      if (mHoleRadiusPercent < 1) {
        this.mHoleRadiusPercent = mHoleRadiusPercent
      } else {
        this.mHoleRadiusPercent = 0.5
      }

      return this
    }

    getLegend(): Legend{
      return this.legend
    }

    // 透明圈颜色设置
    setTransparentCircleColor(color: number): Model {
      this.circleColor = color
      return this
    }

    setDrawHoleEnabled(enabled: boolean): Model{
      this.mDrawHole = enabled
      return this
    }

    getRawRotationAngle(): number {
      return this.mRawRotationAngle
    }

    // 计算孔的路径
    calHolePath(): void {
      let innerRadius = Utils.convertDpToPixel(this.radius * this.mHoleRadiusPercent)
      if (this.mDrawHole) {
        this.innerPath = "M " + innerRadius + " 0" + " " + "A " + innerRadius + " " +
        innerRadius + ", 0, " + 1 + ", 1, " + innerRadius + " " + -0.01
      }
    }

    setGestureStartAngle(x: number, y: number): Model {
      this.mStartAngle = this.getAngleForPoint(x, y) - this.getRawRotationAngle();
      return this
    }

    /**
     * updates the view rotation depending on the given touch position, also
     * takes the starting angle into consideration
     *
     * @param x
     * @param y
     */
    updateGestureRotation(x: number, y: number) {
      this.setRotationAngle(this.getAngleForPoint(x, y) - this.mStartAngle);
    }

    public init(): void {
      this.outerPaths = []
      let pieChartRenderer = new MyPieChartRender(this.mAnimator, this.mViewPortHandler, this);
      let pathPaint = pieChartRenderer.drawData();
      this.outerPaths = pathPaint
      this.legendInit()
      this.legendModel.refresh()
    }

    public legendInit() {
      let entries: JArrayList<LegendEntry> = new JArrayList<LegendEntry>();
      for (let i = 0; i < this.pieData.getDataSets().size(); i++) {
        let dataSet = this.pieData.getDataSetByIndex(i)
        for (var j = 0;j < dataSet.getEntryCount(); j++) {
          let entry = new LegendEntry(
          dataSet.getEntryForIndex(j).getLabel(), // 设置图例的字符串,mLabel
          dataSet.getForm(), // 设置图例的形状,mShape,默认值LegendForm.SQUARE
          dataSet.getFormSize(), // 图例大小,mFormSize,默认值8
          dataSet.getFormLineWidth(), // 图例线宽,mFormLineWidth,默认值3
            null, // 设置虚线,dataSet.getFormLineDashEffect()
          dataSet.getColor(j) // 设置图例图形的颜色,
          )
          entries.add(entry)
        }
      }
      this.legend.setTextSize(12);
      this.legend.setCustom(entries);
    }

    getAngleForPoint(x: number, y: number): number{
      let tx: number = x - this.offset.x - this.extraOffsetXDistance
      let ty: number = y - this.offset.y - this.extraOffsetYDistance
      let length: number = Math.sqrt(tx * tx + ty * ty);
      let r: number = Math.acos(ty / length);
      let angle: number = r * 180.0 / Math.PI;
      if (x > this.offset.x)
      angle = 360 - angle;
      // add 90° because chart starts EAST
      angle = angle + 90;
      // neutralize overflow
      if (angle > 360)
      angle = angle - 360;
      return angle
    }

    drawMarkers(angle: number, x: number, y: number) {
      // 判断点击的位置是否在某个范围内
      let isValueSelected: boolean = false
      let tx: number = x - this.offset.x - this.extraOffsetXDistance
      let ty: number = y - this.offset.y - this.extraOffsetYDistance

      let length: number = Math.sqrt(tx * tx + ty * ty);

      let holeLength = this.mDrawHole ? this.radius * this.mHoleRadiusPercent : 0;

      if (length <= this.radius && length > holeLength) {
        isValueSelected = true
      } else {
        isValueSelected = false
      }
	  
      if (isValueSelected) {
        let angleCount: number = this.mRotationAngle
        for (let i = 0; i < this.drawAngles.length; i++) {
          if (this.mRotationAngle != 0 && angle <= this.mRotationAngle) {
            angle = angle + 360
          }
          angleCount = angleCount + this.drawAngles[i]
          if (angleCount > angle) {
            // 双击取消高亮
            if (this.count == i) {
              this.count = -1
            } else {
              this.count = i
            }
            break
          }
        }
        this.mIndicesToHighlight = this.count
      }
    }
  }
}

export default PieChart